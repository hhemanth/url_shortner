#README

#Deployment
https://limitless-dawn-8758.herokuapp.com/shorturls/

#V0 of app
- Facility to enter a URL and get a shortened url
- add model to store long url and short url
- add controller action to do a 301 redirect
- Display all URLs

#V1 of the app
- Form to add URL.
- Adding Copy to clipboard funtionality
- Adding validations 

#V2 of the app
- Bootstrap integration
- Datatables integration to show all URLs.
- Simple styling

#V3 of the app
-REDIS integration

#V4
-Cleanup

#Current Status Of the app
- Is able to genarate shortened URL for a given URL
- Storing URL in db and in REDIS.
- For a scaled up version of the URL shortner, for example 5-10 million URLs , DB look ups will be slow, and REDIS lookups are order of magnitude faster. 
- Storing data in REDIS allows us to store different 

#Improvements
- UI definitely needs improvement
- Test cases need to be written