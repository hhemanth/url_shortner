class ShorturlsController < ApplicationController
	def index
		@all_urls = ShortUrl.all
	end

	def url_redirect
		lurl = $redis.get(params[:short_url])
		redirect_to lurl
	end

	def create
		@all_urls = ShortUrl.all
		@longUrl = params[:shorturl][:longUrl]
		@smallUrl = ShortUrl.generate_surl
		@surl = ShortUrl.new
		@surl.longUrl = @longUrl
		@surl.smallUrl = @smallUrl
		if @surl.save
			$redis.set(@smallUrl,@longUrl)
    		redirect_to shorturl_path(@surl)
  		else
    		render 'new'
  		end
	end

	def new
		@all_urls = ShortUrl.all
	end

	def edit
	end

	def update
	end

	def destroy
	end

	def show
		@short_url = ShortUrl.find(params[:id])
	end
end
