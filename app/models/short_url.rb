class ShortUrl < ActiveRecord::Base
	validates :longUrl, presence: true
	def self.generate_surl
		(0..9).to_a.shuffle[0,5].join
	end
end
