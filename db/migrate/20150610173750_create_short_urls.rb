class CreateShortUrls < ActiveRecord::Migration
  def change
    create_table :short_urls do |t|
      t.string :longUrl
      t.string :smallUrl

      t.timestamps null: false
    end
  end
end
